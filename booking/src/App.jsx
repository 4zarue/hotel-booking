import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import Home from './components/Home';
import Navigation from './components/Navigation';
import Support from './components/Support';
import MyReservations from './components/MyReservations';
import Login from './components/Login';
import HotelDetail from './components/HotelDetail';
import './App.css';

const App = () => {
  const [loggedInUser, setLoggedInUser] = useState(null);

  useEffect(() => {
    const storedUser = localStorage.getItem('loggedInUser');
    if (storedUser) {
      setLoggedInUser(JSON.parse(storedUser));
    }
  }, []);

  const handleLogin = (user) => {
    setLoggedInUser(user);
  };

  return (
    <>
      <Router>
        <div>
          <Navigation />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/support" element={<Support />} />
            <Route
              path="/my-reservations"
              element={loggedInUser ? <MyReservations userId={loggedInUser.userId} /> : null}
            />
            <Route path="/login" element={<Login onLogin={handleLogin} />} />
            <Route path="/hotel/:hotelId" 
            element={loggedInUser ? <HotelDetail userId={loggedInUser.userId} /> : null}
            />
          </Routes>
        </div>
      </Router>
    </>
  );
};

export default App;
