import { useState, useEffect } from 'react';
import { Table } from 'react-bootstrap';
import PropTypes from 'prop-types';
import db from '../db';

const MyReservations = ({ userId }) => {
  const [reservations, setReservations] = useState([]);

  useEffect(() => {
    const userReservations = db.reservations.filter(reservation => reservation.userId === userId);
    setReservations(userReservations);
  }, [userId]);

  return (
    <div>
      <h2>Your Reservations</h2>
      {reservations.length === 0 ? (
        <p>No reservations found.</p>
      ) : (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Reservation ID</th>
              <th>Hotel ID</th>
            </tr>
          </thead>
          <tbody>
            {reservations.map((reservation, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{reservation.hotelId}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}
    </div>
  );
}

MyReservations.propTypes = {
  userId: PropTypes.number.isRequired,
};

export default MyReservations;
