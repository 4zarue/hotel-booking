import { useState } from "react";
import { Form, Button } from "react-bootstrap";
import './Support.css'

const Support = () => {
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const [text, setText] = useState('');
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log('Email:', email);
    console.log('Phone:', phone);
    console.log('text:', text);
  }

    return (
      <div>
        <div className="Support">
          <h1>Support</h1>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="formBasicPhone">
              <Form.Label>Phone</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter phone number"
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
              />
            </Form.Group>
          <div className="Text">
            <Form.Group controlId="formBasicText">
              <Form.Label>What seems to be the problem?</Form.Label>
              <Form.Control
                type="text"
                placeholder="Write here..."
                value={text}
                onChange={(e) => setText(e.target.value)}
              />
            </Form.Group>
          </div>
            <Button variant="primary" type="submit">
              Submit
            </Button>
          </Form>
        </div>
      </div>
    );
  }
  
export default Support