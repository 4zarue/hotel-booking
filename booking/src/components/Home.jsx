import { useState } from "react";
import { Link } from "react-router-dom";
import SearchBar from "./SearchBar";
import db from "../db";

const HomePage = () => {
  const [filteredHotels, setFilteredHotels] = useState([]);

  const handleSearch = (searchCity) => {
    const filtered = db.hotels.filter(
      (hotel) => hotel.city.toLowerCase() === searchCity.toLowerCase()
    );
    setFilteredHotels(filtered);
  };

  return (
    <div>
      <SearchBar onSearch={handleSearch} />
      <div>
        {filteredHotels.map((hotel) => (
          <div key={hotel.id}>
            <p><Link to={`/hotel/${hotel.id}`}>{hotel.name}</Link> in {hotel.city}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default HomePage;
