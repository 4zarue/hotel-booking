import { useState } from "react";
import PropTypes from "prop-types";
import './SearchBar.css'

const SearchBar = ({ onSearch }) => {
    const [city, setCity] = useState('');
    const handleSearch = () => {
        onSearch(city);
    }

    return (
        <div className="SearchBar">
            <h1>Search hotels</h1>
            <input
                type="text"
                placeholder="City"
                value={city}
                onChange={(e) => setCity(e.target.value)}
            />
            <button onClick={handleSearch}>Search</button>
        </div>
    )
}


SearchBar.propTypes = {
    onSearch: PropTypes.func.isRequired,
  };  

export default SearchBar