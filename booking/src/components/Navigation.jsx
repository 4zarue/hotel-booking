import { Link } from 'react-router-dom';
import './Navigation.css'

const Navigation = () => {
    return (
      <div>
        <nav>
          <Link to="/" className='home'>Hotels</Link>
          <Link to="/support">Support</Link>
          <Link to="/my-reservations">My reservations</Link>
          <Link to="/login" className='login-button'>Log in</Link>
        </nav>
      </div>
    )
}

export default Navigation