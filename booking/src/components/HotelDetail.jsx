import { useState } from 'react';
import { useParams } from 'react-router-dom';
import PropTypes from 'prop-types';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import hotelPic from './hotelPic.jpg';
import dataObject from '../db';
import './HotelDetail.css';

function HotelDetail({ loggedInUser }) {
  const { hotelId } = useParams();
  const thisHotel = dataObject.hotels.find((hotel) => hotel.id === Number(hotelId));

  const [selectedRoom, setSelectedRoom] = useState('');

  const handleRoomSelection = (event) => {
    setSelectedRoom(event.target.value);
  };
console.log(loggedInUser)
  const handleBookRoom = () => {
    if (loggedInUser) {
      console.log(`Room ${selectedRoom} booked!`);
    } else {
      console.log('You need to log in first');
    }
  };

  return (
    <div>
      <h1>{thisHotel.name}</h1>
      <img className="Img" src={hotelPic} alt="Hotel"></img>
      <p>Price: {thisHotel.price} €</p>
      <p>{thisHotel.description}</p>
      <p>Rooms:</p>

      <Form.Select
        className="Select"
        size="lg"
        backgroundColor="gainsboro"
        onChange={handleRoomSelection}
        value={selectedRoom}
      >
        {thisHotel.rooms.map((room, index) => (
          <option key={index}>{room.roomNumber}</option>
        ))}
      </Form.Select>
      <div className="Booking">
        <Button variant="primary" type="submit" onClick={handleBookRoom}>
          {loggedInUser ? `Book a room` : 'Log in to book'}
        </Button>
      </div>
    </div>
  );
}

HotelDetail.propTypes = {
  loggedInUser: PropTypes.object,
};

export default HotelDetail;
