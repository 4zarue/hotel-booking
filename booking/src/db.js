export default {
  hotels: [
    { "id": 1, "name": "Hotel 1", "city": "Rhodes", "description": "Hotel in Rhodes.", "price": 73, "rooms": [
      { "roomNumber": 1 },
      { "roomNumber": 2 }
      ] 
    },
    { "id": 2, "name": "Hotel 2", "city": "Tenerife", "description": "Hotel in Tenerife.", "price": 131, "rooms": [
      { "roomNumber": 1 },
      { "roomNumber": 2 },
      { "roomNumber": 3 }
      ] 
    },
    { "id": 3, "name": "Hotel 3", "city": "Tenerife", "description": "Hotel in Tenerife.", "price": 332, "rooms": [
      { "roomNumber": 1 },
      { "roomNumber": 2 }
      ] 
    }
  ],
  users: [
    { "email": "example@email.com", "password": "Kissa123", "userId": 1 }
  ],
  reservations: [
    { "userId": "", "hotelId": "" }
  ]
}